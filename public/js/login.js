$(function() {

    $('#login-form').submit(function(e){
        e.preventDefault();
        
        var that        = this;
        var el_email    = $(that).find('input[name="email"]');
        var el_password = $(that).find('input[name="password"]');
        
        $.ajax({
            url: 'func/auth/login',
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            type: 'POST',
            data: $(that).serialize(),
            dataType: 'JSON',
            success: function(data) {
                $(that).before(alertMessage(data.success, 'success'));
                $(that)[0].reset();
                setTimeout(() => {
                    location.reload();
                }, 1000);
                },
            error: function(data) {
                var data = data.responseJSON;

                if(data.messages) {
                    var messages = data.messages;

                    if(messages.error) {
                        $(that).before(alertMessage(messages.error, 'danger'));
                    }

                    fieldValidation(el_email, messages.email);
                    fieldValidation(el_password, messages.password);
                }
            },
            beforeSend: function() {
                $('.invalid-feedback').remove();
                $('.alert').remove();
                $('.form-control').removeClass('is-invalid');
                $(that).find('button[type="submit"]').attr('disabled', 'disabled');
                $(that).find('button[type="submit"]').html('Login <i class="fa fa-spin fa-spinner"></i>');
            },
            complete: function() {
                $(that).find('button[type="submit"]').attr('disabled', false);
                $(that).find('button[type="submit"]').html('Login');
            }
        });
    });

});

function alertMessage(message, type) {
    var alert = '';
    alert += '<div class="alert alert-'+ type +' alert-dismissible fade show" role="alert">';
        alert += message;
        alert += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
            alert += '<span aria-hidden="true">&times;</span>';
        alert += '</button>';
    alert += '</div>';

    return alert;
}

function inputFeedback(message, type) {
    var feedback = '';
    feedback += '<div class="'+ type +'-feedback d-block">';
        feedback += message;
    feedback += '</div>';

    return feedback;
}

function fieldValidation(fieldElement, message) {
    if(message) {
        fieldElement.addClass('is-invalid');
        fieldElement.after(inputFeedback(message, 'invalid'));
    } else {
        fieldElement.removeClass('is-invalid');
    }
}
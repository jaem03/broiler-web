<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('login', 'Home::login');
$routes->get('get-buildings', 'Production::get_buildings');
$routes->post('get-batches', 'Production::get_batches');
$routes->post('get-production', 'Production::get_batch_production');
$routes->post('save-data', 'Production::save_data');

$routes->group('func', function($routes) {
	$routes->group('auth', function($routes) {
		$routes->resource('auth');
		$routes->post('login', 'Auth::login');
		
	});
});

$routes->get('logout', 'Auth::logout');
$routes->post('app-login', 'Auth::app_login');
$routes->post('change-password', 'Worker::change_password');

// Dashboard
$routes->group('dashboard', function($routes) {
	$routes->get('index', 'Dashboard::index');
	$routes->get('supplies', 'Dashboard::supplies');
	$routes->get('sales', 'Dashboard::sales');
	$routes->get('production', 'Dashboard::production');
	$routes->get('financial', 'Dashboard::financial');
	$routes->get('workers', 'Dashboard::workers');
});

// Repors
$routes->group('reports', function($routes) {
	$routes->get('sales-report', 'Reports::sales_reports');
	$routes->get('financial-report', 'Reports::financial_report');
});

// Supplies
$routes->group('supplies', function($routes) {
	$routes->post('add', 'Supplies::add');
	$routes->post('delete/(:any)', 'Supplies::delete/$1');
	$routes->get('edit-supply/(:any)', 'Supplies::edit_supply/$1');
});

// Sales
$routes->group('sales', function($routes) {
	$routes->post('add', 'Sales::add');
	$routes->get('get-sales', 'Sales::get_sales');
	$routes->get('delete/(:any)', 'Sales::delete/$1');
});

// Workers
$routes->group('workers', function($routes) {
	$routes->post('add-worker', 'Worker::add_worker');
	$routes->get('get-sales', 'Sales::get_sales');
	$routes->get('delete/(:any)', 'Worker::delete/$1');
});

// Sales
$routes->group('production', function($routes) {
	$routes->post('add-building', 'Production::add_building');
	$routes->post('add-batch', 'Production::add_batch');
	$routes->post('delete-building/(:any)', 'Production::delete_building/$1');
	$routes->get('view/(:any)', 'Dashboard::view_production/$1');
	$routes->get('batch/(:any)', 'Dashboard::view_batch/$1');
	$routes->get('get-buildings', 'Production::get_buildings');
	$routes->get('view-production/(:any)', 'Dashboard::view_batch_production/$1');
	$routes->post('delete-building/(:any)', 'Production::delete_building/$1');
	$routes->post('delete-production/(:any)', 'Production::delete_batch_production/$1');
});

// Financials
$routes->group('financial', function($routes) {
	$routes->post('add-batch', 'Production::add_batch');
	$routes->post('delete/(:any)', 'Financial::delete/$1');
	$routes->get('view/(:any)', 'Dashboard::view_production/$1');
	$routes->get('batch/(:any)', 'Dashboard::view_batch/$1');
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

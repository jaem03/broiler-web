<?php

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
use App\Libraries\Password_Hash;
use App\Models\M_Crud;

class Worker extends ResourceController {

    protected $request;
    protected $validation;
    protected $session;
    protected $m_auth;
    protected $password_hash;
    public $crud;

    public function __construct() {
        $this->request          = \Config\Services::request();
        $this->validation       = \Config\Services::validation();
        $this->session          = \Config\Services::session();
        $this->m_auth           = new \App\Models\M_Auth();
        $this->password_hash    = new Password_Hash();
        $this->crud = new M_Crud();
    }
    public function add_worker(){
        $data = array('email' => $this->request->getPost('email'),
                      'password' => $this->password_hash->hash("password"),
                      'firstname' => $this->request->getPost('firstname'),
                      'lastname' => $this->request->getPost('lastname'),
                      'middlename' => $this->request->getPost('middlename'),
                      'phone' => $this->request->getPost('contact_no')
        );
        $condition = array('email' => $this->request->getPost('email')
            );

        $if_exists = $this->crud->retrieve_where('workers' , $condition);
        if($if_exists){
            return redirect()->route('dashboard/workers')->with('error' , "Email already exists");
        
        }else{
            $save = $this->crud->create('workers' , $data);
            if($save){
                return redirect()->route('dashboard/workers')->with('success' , "Worker added successfuly");
            }
        }
         
        
    }
    public function delete($id = NULL){
        $condition = array('id' => $id );
        $delete = $this->crud->delete_where('workers' , $condition);
        if($delete){
            return redirect()->route('dashboard/workers')->with('success' , "Worker deleted successfuly");
        }
    }
    public function get_sales(){
       $get_all  =  $this->crud->retrieve_sum('sales' , 'total_price');
       return $this->response->setJSON($get_all);
    }
    public function change_password(){
        $worder_id = array('email' => $this->request->getPost('user_id'));
        $old_password = array(
                'email' => $this->request->getPost('user_id'),
                'password' => $this->password_hash->hash($this->request->getPost('old_password')));
        $data = array('password'  => $this->password_hash->hash($this->request->getPost('new_password')));
        $checkOldPassword  =  $this->crud->retrieve_single('workers' , $old_password);
        if($checkOldPassword){
            $update_password =  $this->crud->update_where('workers' , $data , $worder_id);
            if($update_password){
             echo json_encode("Password updated successfuly");
            }else{
             echo json_encode("Update Failed");
            }
        }else{
           echo json_encode("Old password did not match");
        }
    }
    public function edit_record(){

        $data = array('password' => $this->password_hash->hash($this->request->getPost('new_password')),
                      'phone' => $this->request->getPost('new_contact_no'),
                      'firstname' => $this->request->getPost('new_firstname'),
                      'user_type' => "Worker",
                      'lastname' => $this->request->getPost('new_lastname'),
                      'middlename' => $this->request->getPost('new_middlename')
        );
        $condition = array('id' => $this->request->getPost('worker_id'));

        $update = $this->crud->update_where('workers' , $data , $condition);
        return redirect()->route('dashboard/workers')->with('success' , "Worker's record updated succesfuly!");
        
        
    }
}
<?php

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
use App\Libraries\Password_Hash;
use App\Models\M_Crud;

class Sales extends ResourceController {

    protected $request;
    protected $validation;
    protected $session;
    protected $m_auth;
    protected $password_hash;
    public $crud;

    public function __construct() {
        $this->request          = \Config\Services::request();
        $this->validation       = \Config\Services::validation();
        $this->session          = \Config\Services::session();
        $this->m_auth           = new \App\Models\M_Auth();
        $this->password_hash    = new Password_Hash();
        $this->crud = new M_Crud();
    }
    public function add(){
        $data = array('buyer_name' => $this->request->getPost('buyer_name'),
                      'quantity' => $this->request->getPost('quantity'),
                      'total_weight' => $this->request->getPost('total_weight'),
                      'kg_price' => $this->request->getPost('kg_price'),
                      'total_price' => $this->request->getPost('total_price')
        );

            $save = $this->crud->create('sales' , $data);
            if($save){
                return redirect()->route('dashboard/sales')->with('success' , "Sales added successfuly");
            }
            else{
                echo "error";
            }
        
    }
    public function delete($id = NULL){
        $condition = array('id' => $id );
        $delete = $this->crud->delete_where('sales' , $condition);
        if($delete){
            return redirect()->route('dashboard/sales')->with('success' , "Record deleted successfuly");
        }
    }
    public function get_sales(){
       $get_all  =  $this->crud->retrieve_sum('sales' , 'total_price');
       return $this->response->setJSON($get_all);
    }
    public function edit_record(){

        $data = array('buyer_name' => $this->request->getPost('new_buyer_name'),
                      'total_weight' => $this->request->getPost('new_total_weight'),
                      'quantity' => $this->request->getPost('new_quantity'),
                      'kg_price' => $this->request->getPost('new_kg_price'),
                      'total_price' => $this->request->getPost('new_total_price')
        );
        $condition = array('id' => $this->request->getPost('sales_id'));

        $exists = $this->crud->retrieve_where('sales' , $data);
        if($exists){
            return redirect()->route('dashboard/sales')->with('error' , "Sales already exists!");
        }else{
            $update = $this->crud->update_where('sales' , $data , $condition);
            return redirect()->route('dashboard/sales')->with('success' , "Sales record updated succesfuly!");
        }
        
    }
    
}
<?php

namespace App\Controllers;
use App\Models\M_Crud;

class Dashboard extends BaseController {
    public $crud;

    public function __construct() {
        $this->crud = new M_Crud();
    }

    public function index() {
        $data = [
            'title'     => 'Dashboard',
            'content'   => 'dashboard/index',
            'js'        => ''
        ];
        $data['batches'] = $this->crud->retrieve('batch');
        return view('template', $data);
    }
    public function supplies() {
        $data = [
            'title'     => 'Supplies',
            'content'   => 'dashboard/supplies',
            'js'        => ''
        ];

        $data['supplies'] = $this->crud->retrieve('supplies');
        return view('template', $data);
    }
    public function workers() {
        $data = [
            'title'     => 'Workers',
            'content'   => 'dashboard/workers',
            'js'        => ''
        ];

        $data['workers'] = $this->crud->retrieve('workers');
        return view('template', $data);
    }
     public function financial() {
        $data = [
            'title'     => 'Financial Management',
            'content'   => 'dashboard/financial',
            'js'        => ''
        ];

        $data['financial'] = $this->crud->retrieve('financial');
        return view('template', $data);
    }
    
    public function sales() {
        $data = [
            'title'     => 'Sales',
            'content'   => 'dashboard/sales',
            'js'        => ''
        ];

        $data['sales'] = $this->crud->retrieve('sales');
        return view('template', $data);
    }
    public function production() {
        $data = [
            'title'     => 'Production',
            'content'   => 'dashboard/production',
            'js'        => ''
        ];

        $data['buildings'] = $this->crud->retrieve('buildings');
        return view('template', $data);
    }
    public function view_production($building) {
        $data = [
            'title'     => 'Production',
            'header'   =>   $building,
            'content'   => 'production/view',
            'js'        => ''
        ];

        $condition = array('building' => $building );
        $data['batch'] = $this->crud->retrieve_where('batch' , $condition);
        return view('template', $data);
    }

    public function view_batch_production($batch) {
        $data = [
            'title'     => 'Batch Production',
            'header'   =>   $batch,
            'content'   => 'production/view_production',
            'js'        => ''
        ];

        $condition = array('batch' => $batch );
        $data['batch'] = $this->crud->retrieve_where('production_record' , $condition);
        return view('template', $data);
    }
    public function view_batch($batch) {
        $data = [
            'title'     => 'Production',
            'header'   =>   $batch,
            'content'   => 'production/view_batch',
            'js'        => ''
        ];

        $data['batch'] = $this->crud->retrieve('batch');
        return view('template', $data);
    }
    
}
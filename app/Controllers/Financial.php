<?php

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
use App\Libraries\Password_Hash;
use App\Models\M_Crud;

class Financial extends ResourceController {

    protected $request;
    protected $validation;
    protected $session;
    protected $m_auth;
    protected $password_hash;
    public $crud;

    public function __construct() {
        $this->request          = \Config\Services::request();
        $this->validation       = \Config\Services::validation();
        $this->session          = \Config\Services::session();
        $this->m_auth           = new \App\Models\M_Auth();
        $this->password_hash    = new Password_Hash();
        $this->crud = new M_Crud();
    }
    public function edit_supply($id = NULL) {
        $data = [
            'title'     => 'Edit Supply',
            'content'   => 'dashboard/edit_supply',
            'js'        => ''
        ];
        $condition = array('id' => $id );
        $data['supplies'] = $this->crud->retrieve_where('supplies' , $condition);
        return view('template', $data);
    }
    public function add(){

        $data = array('item' => $this->request->getPost('item'),
                      'unit' => $this->request->getPost('unit'),
                      'quantity' => $this->request->getPost('quantity'),
                      'item_cost' => $this->request->getPost('item_cost'),
                      'total_cost' => $this->request->getPost('total_cost')
        );

            $save = $this->crud->create('financial' , $data);
            if($save){
                return redirect()->route('dashboard/financial')->with('success' , "Financial record added successfuly");
            }
        
    }
    public function delete($id = NULL){
        $condition = array('id' => $id );
        $delete = $this->crud->delete_where('financial' , $condition);
        if($delete){
            return redirect()->route('dashboard/financial')->with('success' , "Financial record deleted successfuly");
        }
    }
    public function delete_batch($id = NULL , $building){
        $condition = array('id' => $id );
        $delete = $this->crud->delete_where('batch' , $condition);
        if($delete){
            return redirect()->to('production/view/'.$building.'')->with('success' , "Batch deleted successfuly");
        }
    }
    public function add_batch(){
        $data = array('building' => $this->request->getPost('building'),
                      'batch' => $this->request->getPost('batch'),
                      'total_chickens' => $this->request->getPost('total_chickens')
        );
        $item = array('batch' => $this->request->getPost('batch'),
                      'building' => $this->request->getPost('building')
        );
        $if_exists = $this->crud->retrieve_where('batch' , $item);
        if($if_exists){
            return redirect()->to('production/view/'.$this->request->getPost('building').'')->with('error' , "Batch already exists");
        }else{
            $save = $this->crud->create('batch' , $data);
            if($save){
                return redirect()->to('production/view/'.$this->request->getPost('building').'')->with('success' , "Batch added successfuly");
            }
        }
    }
    public function edit_record(){

        $data = array('item' => $this->request->getPost('new_item'),
                      'unit' => $this->request->getPost('new_unit'),
                      'quantity' => $this->request->getPost('new_quantity'),
                      'item_cost' => $this->request->getPost('new_item_cost'),
                      'total_cost' => $this->request->getPost('new_total_cost')
        );
        $item = array('item' => $this->request->getPost('new_item'));
        $condition = array('id' => $this->request->getPost('financial_id'));

        $exists = $this->crud->retrieve_where('financial' , $data);
        if($exists){
            return redirect()->route('dashboard/financial')->with('error' , "Item already exists!");
        }else{
            $update = $this->crud->update_where('financial' , $data , $condition);
            return redirect()->route('dashboard/financial')->with('success' , "Financial record updated succesfuly!");
        }
        
    }
}
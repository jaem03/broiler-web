<?php

namespace App\Controllers;
use App\Models\M_Crud;

class Reports extends BaseController {
    public $crud;

    public function __construct() {
        $this->crud = new M_Crud();
    }

    public function index() {
        $data = [
            'title'     => 'Dashboard',
            'content'   => 'dashboard/index',
            'js'        => ''
        ];

        return view('template', $data);
    }
    public function supplies() {
        $data = [
            'title'     => 'Supplies',
            'content'   => 'dashboard/supplies',
            'js'        => ''
        ];

        $data['supplies'] = $this->crud->retrieve('supplies');
        return view('template', $data);
    }
    public function workers() {
        $data = [
            'title'     => 'Workers',
            'content'   => 'dashboard/workers',
            'js'        => ''
        ];

        $data['workers'] = $this->crud->retrieve('workers');
        return view('template', $data);
    }
     public function financial_report() {
        $data = [
            'title'     => 'Financial Report',
            'content'   => 'reports/financial',
            'js'        => ''
        ];

        $data['financial'] = $this->crud->retrieve('financial');
        return view('template', $data);
    }
    
    public function sales_report() {
        $data = [
            'title'     => 'Sales Report',
            'content'   => 'reports/sales',
            'js'        => ''
        ];

        $data['sales'] = $this->crud->retrieve('sales');
        return view('template', $data);
    }
}
<?php

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
use App\Libraries\Password_Hash;
use App\Models\M_Crud;

class Production extends ResourceController {

    protected $request;
    protected $validation;
    protected $session;
    protected $m_auth;
    protected $password_hash;
    public $crud;

    public function __construct() {
      
        $this->request          = \Config\Services::request();
        $this->validation       = \Config\Services::validation();
        $this->session          = \Config\Services::session();
        $this->m_auth           = new \App\Models\M_Auth();
        $this->password_hash    = new Password_Hash();
        $this->crud = new M_Crud();
        
    }
    public function edit_supply($id = NULL) {
        $data = [
            'title'     => 'Edit Supply',
            'content'   => 'dashboard/edit_supply',
            'js'        => ''
        ];
        $condition = array('id' => $id );
        $data['supplies'] = $this->crud->retrieve_where('supplies' , $condition);
        return view('template', $data);
    }
    public function add_building(){
        $data = array('building' => $this->request->getPost('building_name')
        );
        $item = array('building' => $this->request->getPost('building_name'));
        $if_exists = $this->crud->retrieve_where('buildings' , $item);
        if($if_exists){
            return redirect()->route('dashboard/production')->with('error' , "Building already exists");
        }else{
            $save = $this->crud->create('buildings' , $data);
            if($save){
                return redirect()->route('dashboard/production')->with('success' , "Building added successfuly");
            }
        }
    }
    public function delete_building($id = NULL){
        $condition = array('id' => $id );
        $delete = $this->crud->delete_where('buildings' , $condition);
        if($delete){
            return redirect()->route('dashboard/production')->with('success' , "Building deleted successfuly");
        }
    }
    public function delete_batch($id = NULL , $building){
        $condition = array('id' => $id );
        $delete = $this->crud->delete_where('batch' , $condition);
        if($delete){
            return redirect()->to('production/view/'.$building.'')->with('success' , "Batch deleted successfuly");
        }
    }
    public function delete_batch_production($id = NULL , $batch = NULL ){
        $condition = array('id' => $id);
        $delete = $this->crud->delete_where('production_record' , $condition);
        if($delete){
            return redirect()->to('production/view-production/'.$batch.'')->with('success' , "Record deleted successfuly");
        }
    }
    public function add_batch(){
        $data = array('building' => $this->request->getPost('building'),
                      'batch' => $this->request->getPost('batch'),
                      'total_chickens' => $this->request->getPost('total_chickens')
        );
        $item = array('batch' => $this->request->getPost('batch'),
                      'building' => $this->request->getPost('building')
        );
        $if_exists = $this->crud->retrieve_where('batch' , $item);
        if($if_exists){
            return redirect()->to('production/view/'.$this->request->getPost('building').'')->with('error' , "Batch already exists");
        }else{
            $save = $this->crud->create('batch' , $data);
            if($save){
                return redirect()->to('production/view/'.$this->request->getPost('building').'')->with('success' , "Batch added successfuly");
            }
        }
    }
    public function get_buildings(){
    
        $get_all  =  $this->crud->retrieve('buildings');
        return $this->response->setJSON($get_all);
    }
    public function get_batches(){
       header('Access-Control-Allow-Origin: *');
       header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
       $condition = array('building' => $this->request->getPost('buildingId')) ;
       $get_all  =  $this->crud->retrieve_where('batch' , $condition);
       return $this->response->setJSON($get_all);
    }
    public function get_batch_production(){
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $condition = array('batch' => $this->request->getPost('batch')) ;
        $get_all  =  $this->crud->retrieve_where('production_record' , $condition);
        return $this->response->setJSON($get_all);
     }
     public function save_data(){
        $inputData = [
            'building'     => $this->request->getPost('building'),
            'worker'     => $this->request->getPost('worker'),
            'harvested_chickens'     => $this->request->getPost('harvested_chickens'),
            'batch'     => $this->request->getPost('batch'),
            'mortality'     => $this->request->getPost('mortality'),
            'feed_consumption'     => $this->request->getPost('feed'),
            'remarks'     => $this->request->getPost('remarks')
        ];
        
        $save = $this->crud->create('production_record' , $inputData);
        if($save){
            return $this->response->setJSON(1);
        }else{
            return $this->response->setJSON(0);
        }
     }
     public function edit_building(){
         $data = array('building' => $this->request->getPost('new_building_name'));
         $condition = array('id' => $this->request->getPost('building_id'));
        

         $exists = $this->crud->retrieve_where('buildings' , $data);
         if($exists){
            return redirect()->route('dashboard/production')->with('error' , "Building name already exists!");
         }else{
            $update = $this->crud->update_where('buildings' , $data , $condition);
            if($update){
                return redirect()->route('dashboard/production')->with('success' , "Building updated successfuly");
             }
         }

         
     }
}
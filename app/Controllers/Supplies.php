<?php

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
use App\Libraries\Password_Hash;
use App\Models\M_Crud;

class Supplies extends ResourceController {

    protected $request;
    protected $validation;
    protected $session;
    protected $m_auth;
    protected $password_hash;
    public $crud;

    public function __construct() {
        $this->request          = \Config\Services::request();
        $this->validation       = \Config\Services::validation();
        $this->session          = \Config\Services::session();
        $this->m_auth           = new \App\Models\M_Auth();
        $this->password_hash    = new Password_Hash();
        $this->crud = new M_Crud();
    }
    public function edit_supply($id = NULL) {
        $data = [
            'title'     => 'Edit Supply',
            'content'   => 'dashboard/edit_supply',
            'js'        => ''
        ];
        $condition = array('id' => $id );
        $data['supplies'] = $this->crud->retrieve_where('supplies' , $condition);
        return view('template', $data);
    }
    public function add(){
        $data = array('item' => $this->request->getPost('item'),
                      'stock' => $this->request->getPost('stock')
        );
        $item = array('item' => $this->request->getPost('item'));
        $if_exists = $this->crud->retrieve_where('supplies' , $item);
        if($if_exists){
            return redirect()->route('dashboard/supplies')->with('error' , "Supply already exists");
        }else{
            $save = $this->crud->create('supplies' , $data);
            if($save){
                return redirect()->route('dashboard/supplies')->with('success' , "Supply added successfuly");
            }
        }
    }
    public function delete($id = NULL){
        $condition = array('id' => $id );
        $delete = $this->crud->delete_where('supplies' , $condition);
        if($delete){
            return redirect()->route('dashboard/supplies')->with('success' , "Supply deleted successfuly");
        }
    }
    public function edit_record(){

        $data = array('item' => $this->request->getPost('new_item'),
                      'stock' => $this->request->getPost('new_stock')
        );

        $condition = array('id' => $this->request->getPost('supply_id'));

        $exists = $this->crud->retrieve_where('supplies' , $data);
        if($exists){
            return redirect()->route('dashboard/supplies')->with('error' , "Supply already exists!");
        }else{
            $update = $this->crud->update_where('supplies' , $data , $condition);
            return redirect()->route('dashboard/supplies')->with('success' , "Supply updated succesfuly!");
        }
        
    }
}
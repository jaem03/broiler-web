<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$data = [
			'title' => 'Login'
		];

		return view('login_template', $data);
	}

	public function login() {
		$data = [
			'title' => 'Login'
		];

		return view('login_template', $data);
	}
}

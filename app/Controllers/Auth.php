<?php

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
use App\Libraries\Password_Hash;
use App\Models\M_Crud;



class Auth extends ResourceController {

    protected $request;
    protected $validation;
    protected $session;
    protected $m_auth;
    protected $password_hash;
    public $crud;


    public function __construct() {
        $this->request          = \Config\Services::request();
        $this->validation       = \Config\Services::validation();
        $this->session          = \Config\Services::session();
        $this->m_auth           = new \App\Models\M_Auth();
        $this->password_hash    = new Password_Hash();
        $this->crud = new M_Crud();
    }

    private function save_login_session($data) {
        $sess_data = array(
            'sess_uid'         => $data['id'],
            'sess_email'       => $data['email'],
            'sess_firstname'   => $data['firstname'],
            'sess_middlename'  => $data['middlename'],
            'sess_lastname'    => $data['lastname'],
            'sess_user_type'   => $data['user_type'],
            'sess_logged_in'   => TRUE
        );

        return $this->session->set($sess_data);
   }

    public function login() {
        $inputData = [
            'email'     => $this->request->getPost('email'),
            'password'  => $this->password_hash->hash($this->request->getPost('password'))
        ];

        $this->validation->withRequest($this->request)->setRules([
            'email' => [
                'label'     => 'Email',
                'rules'     => 'required',
                'errors'    => [
                    'required' => '{field} is required.'
                ]
            ],
            'password' => [
                'label'     => 'Password',
                'rules'     => 'required',
                'errors'    => [
                    'required' => '{field} is required.'
                ]
            ]
        ]);

        if ( $this->validation->withRequest($this->request)->run() ) {
            $login = $this->m_auth->login($inputData);
            if( count($login) ) {
                $this->save_login_session($login);
                return redirect()->route('dashboard/sales')->with('success' , "Welcome");
            } else {

                // return $this->fail([
                //     'error' => 'Email or password is incorrect.'
                // ]);
             return redirect()->route('login')->with('error' , "Username or Password is incorrect.");
            }
        } else {
            return redirect()->route('login');
        }
    }
    public function app_login() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
   
        $inputData = [
            'email'     => $this->request->getPost('email'),
            'password'  => $this->password_hash->hash($this->request->getPost('password'))
        ];
        $login = $this->crud->retrieve_single('workers' , $inputData);
        if($login){
            return $this->response->setJSON($login);
        }else{
            return $this->response->setJSON(0);
        }
         
    }
    public function register(){
        $inputData = [
            'email'     => "user@gmail.com",
            'password'  => $this->password_hash->hash("password")
        ];
        $register = $this->crud->create('users' , $inputData);
    }
    public function logout(){
        $logout = $this->session->destroy();
        return redirect()->to(base_url());
    }
}
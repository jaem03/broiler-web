<?php

namespace App\Libraries;

class Password_Hash {
    public function hash($password) {
        $key 				= 'aBigsecret_ofAtleast32Characters';		
		$salt3				= '!@GH^%)-:HDF%';
		$salt2				= hash('sha512', $key, $password . $salt3);
		$salt1				= hash('sha512', $password . $key . $salt2);
		$hashed_password	= hash('sha512', $salt1 . $password . $salt2 . $salt3);
		$password			= $hashed_password;

        return $password;
    }
}
<?php

namespace App\Models;
use CodeIgniter\Model;

class M_crud extends Model
{
    public function retrieve_where($table , $data){
        $db      = \Config\Database::connect();
        $builder = $db->table($table);
        $builder->where($data);
        $query   = $builder->get();
        return $query->getResult();
    }
    public function retrieve($table){
        $db      = \Config\Database::connect();
        $builder = $db->table($table);
        $query   = $builder->get();
        return $query->getResult();
    }
    public function retrieve_single($table , $data){
        $db      = \Config\Database::connect();
        $builder = $db->table($table);
        $builder->where($data);
        $query   = $builder->get();
        return $query->getResult();
    }
    public function create($table , $data){
        $db      = \Config\Database::connect();
        $builder = $db->table($table);
        $query   = $builder->insert($data);
        return $query;
    }
    public function update_where($table , $data , $condition){
        $db      = \Config\Database::connect();
        $builder = $db->table($table);
        $builder->where($condition);
        $query   = $builder->update($data);
        return $query;
    }
    public function delete_where($table ,  $condition){
        $db      = \Config\Database::connect();
        $builder = $db->table($table);
        $builder->where($condition);
        $query   = $builder->delete();
        return $query;
    }
      public function retrieve_sum($table , $sum){
        $db      = \Config\Database::connect();
        $builder = $db->table($table);
        $builder->select("created_at");
        $builder->groupBy("created_at");
        $builder->selectSum($sum);
        $query   = $builder->get();
        return $query->getResult();
    }
}

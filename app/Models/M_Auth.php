<?php

namespace App\Models;
use CodeIgniter\Model;

class M_Auth extends Model {
    protected $db;
    protected $encrypter;

    public function __construct() {
        $this->db  = \Config\Database::connect();
    }

public function login($inputData) {
        $data = [];

        $email      = $inputData['email'];
        $password   = $inputData['password'];

        $qString    = "SELECT * FROM users WHERE email = :email: AND password = :password: LIMIT 1";
        $query      = $this->db->query($qString, [
            'email'     => $email,
            'password'  => $password
        ]);

        if ( $query->getNumRows() > 0 ) {
            $data = $query->getRowArray();
        }

        $query->freeResult();
        return $data;
    }
    public function register($inputData) {
        $data = [];

        $email      = $inputData['email'];
        $password   = $inputData['password'];

        $qString    = "SELECT * FROM users WHERE email = :email: AND password = :password: LIMIT 1";
        $query      = $this->db->query($qString, [
            'email'     => $email,
            'password'  => $password
        ]);

        if ( $query->getNumRows() > 0 ) {
            $data = $query->getRowArray();
        }

        $query->freeResult();
        return $data;
    }
}
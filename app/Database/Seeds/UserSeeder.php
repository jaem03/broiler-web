<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UserSeeder extends Seeder
{
	public function run()
	{
		$password = 'hArlxpw0723';

		$key 				= 'aBigsecret_ofAtleast32Characters';		
		$salt3				= '!@GH^%)-:HDF%';
		$salt2				= hash('sha512', $key, $password . $salt3);
		$salt1				= hash('sha512', $password . $key . $salt2);
		$hashed_password	= hash('sha512', $salt1 . $password . $salt2 . $salt3);
		$password			= $hashed_password;

		$data = [
			'email'			=> 'lumagui.harl0723@gmail.com',
			'password'		=> $password,
			'phone'			=> '09156309799',
			'firstname'		=> 'Harley',
			'middlename'	=> 'Ferrer',
			'lastname'		=> 'Lumagui',
			'user_type'		=> 'Administrator',
		];

		$this->db->table('users')->insert($data);
	}
}

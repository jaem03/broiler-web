<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type'				=> 'INT',
				'auto_increment'	=> TRUE
			],			
			'email' => [
				'type'				=> 'VARCHAR',
				'constraint'		=> 100
			],
			'password' => [
				'type'				=> 'TEXT'
			],
			'phone' => [
				'type'				=> 'VARCHAR',
				'constraint'		=> 20
			],
			'firstname' => [
				'type'				=> 'VARCHAR',
				'constraint'		=> 255
			],
			'middlename' => [
				'type'				=> 'VARCHAR',
				'constraint'		=> 255
			],
			'lastname' => [
				'type'				=> 'VARCHAR',
				'constraint'		=> 255
			],
			'user_type' => [
				'type'				=> 'VARCHAR',
				'constraint'		=> 25
			],
			'created_at datetime default current_timestamp',
			'updated_at datetime default current_timestamp on update current_timestamp'
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('users');
	}

	public function down()
	{
		$this->forge->dropTable('users');
	}
}

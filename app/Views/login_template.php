<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?= $title ?></title>

    <!-- Bootstrap -->
    <link href="<?= base_url() ?>/assets/template/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?= base_url() ?>/assets/template/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?= base_url() ?>/assets/template/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?= base_url() ?>/assets/template/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?= base_url() ?>/assets/template/build/css/custom.min.css" rel="stylesheet">
</head>
<body class="login">

<div>
      <div class="login_wrapper">
          
        <div class="animate form login_form">
          <section class="login_content">
             <?php if (session()->getFlashdata('success') !== NULL) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('success') ?>
            </div>
        <?php endif; ?>
        <?php if (session()->getFlashdata('error') !== NULL) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('error') ?>
                </div>
            <?php endif; ?>
            <form id="login-form" method = "post" action = "<?= base_url('auth/login') ?>">
              <h1>Login</h1>
              <div class="form-group">
                <input required type="email" name="email" class="form-control" placeholder="Email" />
              </div>
              <div class="form-group">
                <input required type="password" name="password" class="form-control" placeholder="Password" />
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-secondary submit">Log in</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1>       <img width = "40" src="<?= base_url('assets/images/chicken.png') ?>" alt="" srcset="">
                      Broiler Smart Manager</h1>
                  <p>©<?= date('Y') ?> All Rights Reserved. Web and Mobile Application for Poultry Farm Management in San Miguel, Bulacan</p>
                </div>
              </div>
            </form>
          </section>
        </div>

      </div>
    </div>

    <!-- jQuery -->
    <script src="<?= base_url() ?>/assets/template/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?= base_url() ?>/assets/template/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url() ?>/assets/template/js/login.js"></script>
    
</body>
</html>
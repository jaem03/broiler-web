

        <!-- page content -->
    <div class="right_col" role="main">
            <button class = "btn btn-success" data-toggle = "modal" data-target = "#add-supply">Add new Supply</button>
            <div class="container">
            <?php if (session()->getFlashdata('success') !== NULL) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('success') ?>
            </div>
        <?php endif; ?>
        <?php if (session()->getFlashdata('error') !== NULL) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('error') ?>
            </div>
        <?php endif; ?>
        <div class = "table-responsive">
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Item</th>
				<th>Stock</th>
                <th>Date Added</th>
				<th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($supplies as $supp){ ?>
                <tr>
                    <td><?= $supp->id ?></td>
                    <td><?= $supp->item ?></td>
                    <td><?= $supp->stock ?></td>
                    <td><?= $supp->created_at ?></td>
                    <td>
                        <a class = "btn btn-sm btn-danger" href= "<?= base_url('supplies/delete/'.$supp->id.'') ?>"> <i class = "fa fa-trash"></i> </a>
                        <button class = "btn btn-sm btn-warning"> <i class = "fa fa-pencil"></i> </button>
                    </td>
                </tr>
            <?php }?>
        </tbody>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Item</th>
				<th>Stock</th>
                <th>Date Added</th>
				<th>Options</th>
            </tr>
        </tfoot>
    </table>
    
    </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- modals -->
        <div class="modal fade" id = "add-supply" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add new Supply</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form id="demo-form2" method= "post" action = "<?= base_url('supplies/add') ?>">
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Item <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="item" name = "item" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Stock <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="number" id="stock" name = "stock" required="required" class="form-control ">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                    </form>
                </div>
                </div>


    <script>
        $(document).ready(function() {
                $('#example').DataTable();
            } );
    </script>



        <!-- page content -->
    <div class="right_col" role="main">
            <button class = "btn btn-success" data-toggle = "modal" data-target = "#add-building">Add new Building</button>
            <div class="container">
            <?php if (session()->getFlashdata('success') !== NULL) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('success') ?>
            </div>
        <?php endif; ?>
        <?php if (session()->getFlashdata('error') !== NULL) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('error') ?>
                </div>
            <?php endif; ?>

    <table border="0" cellspacing="5" cellpadding="5" class = "table-bordered" style = "margin-bottom:10px;">
            <tbody><tr>
                <td>Record From:</td>
                <td><input type="text" id="min" name="min"></td>
            </tr>
            <tr>
                <td>To:</td>
                <td><input type="text" id="max" name="max"></td>
            </tr>
            </tbody>
    </table>
            <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Building</th>
    
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($buildings as $dt){ ?>
                <tr>
                    <td><?= $dt->id ?></td>
                    <td><?= $dt->building ?></td>
                    <td>
                    <a class = "btn btn-sm btn-danger" href= "<?= base_url('production/delete_building/'.$dt->id.'') ?>"> <i class = "fa fa-trash"></i> </a>
                        <button onclick = "edit_button('<?= $dt->id ?>' , '<?= $dt->building ?>')" data-toggle= 'modal' data-building = "<?= $dt->building ?>" data-target = '#edit-building' data-id = "<?= $dt->id ?>" class = "btn btn-sm btn-warning"> <i class = "fa fa-pencil"></i> </button>
                        <a class = "btn btn-sm btn-success" href= "<?= base_url('production/view/'.$dt->building.'') ?>"> View Production Batches</a>
                    </td>
                </tr>
            <?php }?>
        </tbody>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Building</th>
                <th>Options</th>
            </tr>
        </tfoot>
    </table>
            </div>
        </div>
        <!-- /page content -->

        <!-- modals -->
        <div class="modal fade" id = "add-building" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add new Building</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form id="demo-form2" method= "post" action = "<?= base_url('production/add-building') ?>">
                        <div class = "row">
                            <div class="col-md-12 col-sm-12 ">
                                <label for="">Building Name</label>
                                <input type="text" id="building_name" name = "building_name" required="required" class="form-control ">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                    </form>
                </div>
                </div>

                <div class="modal fade" id = "edit-building" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Building</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form id="demo-form2" method= "post" action = "<?= base_url('production/edit_building') ?>">
                        <div class = "row">
                            <div class="col-md-12 col-sm-12 ">
                                <label for="">Building Name</label>
                                <input type="text" id="new_building_name" name = "new_building_name" required="required" class="form-control ">
                                <input type="hidden" id="building_id" name = "building_id" required="required" class="form-control ">
                           
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                    </form>
                </div>
                </div>


    <script>
        var minDate, maxDate;
 
        // Custom filtering function which will search data in column four between two values
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var min = minDate.val();
                var max = maxDate.val();
                var date = new Date( data[2] );
        
                if (
                    ( min === null && max === null ) ||
                    ( min === null && date <= max ) ||
                    ( min <= date   && max === null ) ||
                    ( min <= date   && date <= max )
                ) {
                    return true;
                }
                return false;
            }
        );
        $(document).ready(function() {
                minDate = new DateTime($('#min'), {
                    format: 'MMMM Do YYYY'
                });
                maxDate = new DateTime($('#max'), {
                    format: 'MMMM Do YYYY'
                });
 
            var table =  $('#example').DataTable();
            // Refilter the table
            $('#min, #max').on('change', function () {
                    table.draw();
                });
            });
            
         function total(){
             var qty = $("#quantity").val();
             var kg_price =  $("#kg_price").val();
             var total_weight =  $("#total_weight").val();
             var total = kg_price * total_weight;
             $("#total_price").val(total);

         }
        function edit_button(id , building_name){
            $("#new_building_name").val(building_name);
            $("#building_id").val(id);
        }

    </script>



        <!-- page content -->
    <div class="right_col" role="main">
            <button class = "btn btn-success" data-toggle = "modal" data-target = "#add-financial">Add new Financial Record</button>
            <div class="container">
            <?php if (session()->getFlashdata('success') !== NULL) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('success') ?>
            </div>
        <?php endif; ?>
        <?php if (session()->getFlashdata('error') !== NULL) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('error') ?>
            </div>
        <?php endif; ?>
        <div class = "table-responsive">
              <table border="0" cellspacing="5" cellpadding="5" class = "table-bordered" style = "margin-bottom:10px;">
            <tbody><tr>
                <td>Record From:</td>
                <td><input type="text" id="min" name="min"></td>
            </tr>
            <tr>
                <td>To:</td>
                <td><input type="text" id="max" name="max"></td>
            </tr>
            </tbody>
    </table>
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Item</th>
				<th>Unit</th>
                <th>Quantity</th>
				<th>Item Cost</th>
                <th>Total Cost</th>
                <th>Date Added</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($financial as $dt){ ?>
                <tr>
                    <td><?= $dt->id ?></td>
                    <td><?= $dt->item ?></td>
                    <td><?= $dt->unit ?></td>
                    <td><?= $dt->quantity ?></td>
                    <td><?= $dt->item_cost ?></td>
                    <td><?= $dt->total_cost ?></td>
                    <td><?= $dt->created_at ?></td>
           
                    <td>
                        <a class = "btn btn-sm btn-danger" href= "<?= base_url('financial/delete/'.$dt->id.'') ?>"> <i class = "fa fa-trash"></i> </a>
                        <button onclick = "edit_button('<?= $dt->id ?>' , '<?= $dt->item ?>', '<?= $dt->unit ?>', '<?= $dt->quantity ?>', '<?= $dt->item_cost ?>', '<?= $dt->total_cost ?>')" data-toggle= 'modal' data-target = '#edit-financial' class = "btn btn-sm btn-warning"> <i class = "fa fa-pencil"></i> </button>
                           </td>
                </tr>
            <?php }?>
        </tbody>
         <tfoot>
                        <tr>
                            <th colspan="8" style="text-align:right"></th>
                        </tr>
                    </tfoot>
    </table>
    
    </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- modals -->
        <div class="modal fade" id = "add-financial" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add new Financial Record</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form id="demo-form2" method= "post" action = "<?= base_url('financial/add') ?>">
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Item <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="item" name = "item" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Unit 
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="unit" name = "unit" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Quantity <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="number" id="quantity" name = "quantity" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Item Cost <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="item_cost" name = "item_cost" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Total Cost <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input onfocus = "total()" type="text" id="total_cost" name = "total_cost" required="required" class="form-control ">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                    </form>
                </div>
                </div>

                <div class="modal fade" id = "edit-financial" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Financial Record</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form id="demo-form2" method= "post" action = "<?= base_url('financial/edit_record') ?>">
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Item <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="new_item" name = "new_item" required="required" class="form-control ">
                                <input type="hidden" id="financial_id" name = "financial_id" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Unit 
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="new_unit" name = "new_unit" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Quantity <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="number" id="new_quantity" name = "new_quantity" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Item Cost <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="new_item_cost" name = "new_item_cost" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Total Cost <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input onfocus = "total2()" type="text" id="new_total_cost" name = "new_total_cost" required="required" class="form-control ">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                    </form>
                </div>
                </div>
    <script>
         var minDate, maxDate;
 
        // Custom filtering function which will search data in column four between two values
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var min = minDate.val();
                var max = maxDate.val();
                var date = new Date( data[6] );
        
                if (
                    ( min === null && max === null ) ||
                    ( min === null && date <= max ) ||
                    ( min <= date   && max === null ) ||
                    ( min <= date   && date <= max )
                ) {
                    return true;
                }
                return false;
            }
        );
        $(document).ready(function() {
            minDate = new DateTime($('#min'), {
                    format: 'MMMM Do YYYY'
                });
                maxDate = new DateTime($('#max'), {
                    format: 'MMMM Do YYYY'
                });
 
            var table =  $('#example').DataTable({
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            // Total over all pages
            totalCost = api
                .column(5, { page: 'current'}   )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
 
            // Update footer
            $( api.column(  ).footer() ).html(
                '<span>Overall Cost: </span>'+ totalCost
            );
        }
            });
            // Refilter the table
            $('#min, #max').on('change', function () {
                            table.draw();
                });
            });
  
        function total(){
             var qty = $("#quantity").val();
             var item_cost =  $("#item_cost").val();
             var total = qty * item_cost;
             $("#total_cost").val(total);

         }
         function total2(){
             var qty = $("#new_quantity").val();
             var item_cost =  $("#new_item_cost").val();
             var total = qty * item_cost;
             $("#new_total_cost").val(total);

         }
         function edit_button(id ,item,  unit , quantity , item_cost , total_cost){
            $("#new_unit").val(unit);
            $("#new_item").val(item);
            $("#new_quantity").val(quantity);
            $("#new_item_cost").val(item_cost);
            $("#new_total_cost").val(total_cost);
            $("#financial_id").val(id);
        }
    </script>

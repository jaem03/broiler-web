
        <!-- page content -->
        <div class="right_col" role="main">
     
          <div class="row" style="display: inline-block;">

            <div class=" top_tiles" style="margin: 10px 0;">
            <div class = "container col-md-12">
              <?php if (session()->getFlashdata('success') !== NULL) : ?>
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                      <?php echo session()->getFlashdata('success') ?>
                  </div>
              <?php endif; ?>
              <?php if (session()->getFlashdata('error') !== NULL) : ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <?php echo session()->getFlashdata('error') ?>
                  </div>
              <?php endif; ?>
            </div>
             
            </div>
          </div>
        <div class="row">
          <div class="col-md-12">
            <div class="top_tiles">
                <?php if($batches) {foreach ($batches as $batch) {
                    ?>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 ">
                        <div class="tile-stats">
                        <div class="count">Batch: <?= $batch->batch ?></div>
                        <h3>Chickens: <?= $batch->total_chickens ?></h3>
                        </div>
                    </div>

              <?php
                }} ?>
            </div>
          </div>
        </div>
          <div class="row">
            <div class="col-md-12">
                <div class="card">
                  <div class="card-header bg-secondary text-white">
                      <h3>Sales</h3>
                  </div>
                  <div class="card-body">
                      <figure class="highcharts-figure">
                      <div id="container"></div>
                  </figure>
                  </div>
                </div>
              
          
            </div>
          </div>

            <br/>
        </div>
        </div>
        <!-- /page content -->
    <script>
    $(function () {

        $.getJSON("<?= base_url('sales/get-sales') ?>", function(response) {
            let xAxisLabel=[];
            let yAxisData = [];

              $.each(response, function(i , dt) {
                  
                    month = moment(dt.created_at).format('MMMM');
                    xAxisLabel.push(dt.created_at);
                    yAxisData.push(parseFloat(dt.total_price));
                });


                //const newDate = new Date('Jul 1 16'); //test it parses data correctly
            const myData = xAxisLabel.map((child,index) => {
              const modString = child.replace(/ /, " 1 ");
              const newDate = new Date(modString);
              const month = newDate.getMonth();
              const year = newDate.getFullYear();
              return [Date.UTC(year, month, 1),yAxisData[index]]
            });

        
Highcharts.chart('container', {
		chart: {
        type: 'spline'
    },
    title: {
        text: 'Sales Graph'
    },

    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: { // do display the year
            month: '%b %y',
            year: '%Y'
        },
        title: {
            text: 'Date'
        },
        /*tickInterval: 1*/
        tickPixelInterval: 100
    },
    yAxis: {
        title: {
            text: 'Total Sales'
        },
        min: 0
    },
    series: [{
        name: "Sales",
        data: myData     
    }],
    plotOptions: {
        series: {
            marker: {
                enabled: true
            }
        }
    },
});

        });
      
  });
</script>
 
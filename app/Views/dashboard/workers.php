

        <!-- page content -->
    <div class="right_col" role="main">
            <button class = "btn btn-success" data-toggle = "modal" data-target = "#add-supply">Add new Worker</button>
            <div class="container">
            <?php if (session()->getFlashdata('success') !== NULL) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('success') ?>
            </div>
        <?php endif; ?>
        <?php if (session()->getFlashdata('error') !== NULL) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('error') ?>
            </div>
        <?php endif; ?>
        <div class = "table-responsive">
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Email</th>
                <th>Name</th>
				<th>Role</th>
                <th>Date Added</th>
				<th>Options</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($workers as $dt){ ?>
                <tr>
                    <td><?= $dt->email ?></td>
                    <td><?= $dt->firstname ." ". $dt->middlename . " ". $dt->lastname ?></td>
                    <td><?= $dt->user_type ?></td>
                    <td><?= $dt->created_at ?></td>
                    <td>
                        <a class = "btn btn-sm btn-danger" href= "<?= base_url('workers/delete/'.$dt->id.'') ?>"> <i class = "fa fa-trash"></i> </a>
                        <button onclick = "edit_button('<?= $dt->id ?>', '<?= $dt->email ?>' , '<?= $dt->phone ?>' ,'<?= $dt->firstname ?>', '<?= $dt->lastname ?>', '<?= $dt->middlename ?>')" data-toggle= 'modal' data-target = '#edit-worker' class = "btn btn-sm btn-warning"> <i class = "fa fa-pencil"></i> </button>
                         
                    </td>
                </tr>
            <?php }?>
        </tbody>
        <tfoot>
            <tr>
                <th>Email</th>
                <th>Name</th>
				<th>Role</th>
                <th>Date Added</th>
				<th>Options</th>
            </tr>
        </tfoot>
    </table>
    
    </div>
            </div>
        </div>
        <!-- /page content -->

        <!-- modals -->
        <div class="modal fade" id = "add-supply" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add new Worker</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form id="demo-form2" method= "post" action = "<?= base_url('workers/add-worker') ?>">
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Email <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="email" id="email" name = "email" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Password <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="password" id="password" name = "password" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Contact No <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="contact_no" name = "contact_no" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Firstname <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="firstname" name = "firstname" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Lastname <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="lastname" name = "lastname" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Middlename <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="middlename" name = "middlename" required="required" class="form-control ">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                    </form>
                </div>
                </div>
            <div class="modal fade" id = "edit-worker" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Worker</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form id="demo-form2" method= "post" action = "<?= base_url('worker/edit_record') ?>">
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Email <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input readonly type="email" id="worker_email" name = "worker_email" required="required" class="form-control ">
                                <input  type="hidden" id="worker_id" name = "worker_id" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Password <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="password" id="new_password" name = "new_password" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Contact No <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="new_contact_no" name = "new_contact_no" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Firstname <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="new_firstname" name = "new_firstname" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Lastname <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="new_lastname" name = "new_lastname" required="required" class="form-control ">
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Middlename <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="new_middlename" name = "new_middlename" required="required" class="form-control ">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                    </form>
                </div>
                </div>


    <script>
        $(document).ready(function() {
                $('#example').DataTable();
            });
        function edit_button(id , email,  contact_no , firstname , lastname , middlename){
            $("#worker_id").val(id);
            $("#worker_email").val(email);
            $("#new_contact_no").val(contact_no);
            $("#new_firstname").val(firstname);
            $("#new_lastname").val(lastname);
            $("#new_middlename").val(middlename);
        }
    </script>

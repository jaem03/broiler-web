

        <!-- page content -->
    <div class="right_col" role="main">
      <div class="container">
            <?php if (session()->getFlashdata('success') !== NULL) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('success') ?>
            </div>
        <?php endif; ?>
        <?php if (session()->getFlashdata('error') !== NULL) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('error') ?>
                </div>
            <?php endif; ?>

    <table border="0" cellspacing="5" cellpadding="5" class = "table-bordered" style = "margin-bottom:10px;">
            <tbody><tr>
                <td>Reports From:</td>
                <td><input type="text" id="min" name="min"></td>
            </tr>
            <tr>
                <td>To:</td>
                <td><input type="text" id="max" name="max"></td>
            </tr>
            </tbody>
    </table>
            <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Buyer Name</th>
				<th>Quantity</th>
                <th>Total Weight</th>
				<th>Kilogram Price</th>
                <th>Total Price</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sales as $sl){ ?>
                <tr>
                    <td><?= $sl->id ?></td>
                    <td><?= $sl->buyer_name ?></td>
                    <td><?= $sl->quantity ?></td>
                    <td><?= $sl->total_weight ?></td>
                    <td><?= $sl->kg_price ?></td>
                    <td><?= $sl->total_price ?></td>
                    <td><?= $sl->created_at ?></td>
                </tr>
            <?php }?>
        </tbody>
            <tfoot>
                        <tr>
                            <th colspan="7" style="text-align:right"></th>
                        </tr>
                    </tfoot>
    </table>
            </div>
        </div>
        <!-- /page content -->

        <!-- modals -->
        <div class="modal fade" id = "add-sales" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add new Sales</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form id="demo-form2" method= "post" action = "<?= base_url('sales/add') ?>">
                        <div class = "row">
                            <div class="col-md-12 col-sm-12 ">
                                <label for="">Buyer Name</label>
                                <input type="text" id="buyer_name" name = "buyer_name" required="required" class="form-control ">
                            </div>
                            <div class="col-md-3 col-sm-3 ">
                                <label for="">Quantity</label>
                                <input type="number" id="quantity" name = "quantity" required="required" class="form-control ">
                            </div>
                            <div class="col-md-3 col-sm-3 ">
                                <label for="">Total Weigth</label>
                                <input step =".01" type="number" id="total_weight" name = "total_weight" required="required" class="form-control ">
                            </div>
                            <div class="col-md-3 col-sm-3 ">
                                <label for="">Price/Kilogram</label>
                                <input step =".01" type="number" id="kg_price" name = "kg_price" required="required" class="form-control ">
                            </div>
                            <div class="col-md-3 col-sm-3 ">
                                <label for="">Total Price</label>
                                <input onfocus = "total()" step =".01" type="number" id="total_price" name = "total_price" required="required" class="form-control ">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                    </form>
                </div>
                </div>


    <script>
        var minDate, maxDate;
 
        // Custom filtering function which will search data in column four between two values
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var min = minDate.val();
                var max = maxDate.val();
                var date = new Date( data[6] );
        
                if (
                    ( min === null && max === null ) ||
                    ( min === null && date <= max ) ||
                    ( min <= date   && max === null ) ||
                    ( min <= date   && date <= max )
                ) {
                    return true;
                }
                return false;
            }
        );
        $(document).ready(function() {
            minDate = new DateTime($('#min'), {
                    format: 'MMMM Do YYYY'
                });
                maxDate = new DateTime($('#max'), {
                    format: 'MMMM Do YYYY'
                });
 
            var table =  $('#example').DataTable({
                 dom: 'BPlfrtip', 
                 buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                  "footerCallback": function ( row, data, start, end, display ) {
                        var api = this.api(), data;
                        // Remove the formatting to get integer data for summation
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };
                        // Total over all pages
                        totalSales = api
                            .column(5, { page: 'current'}   )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );
            
            
                        // Update footer
                        $( api.column(  ).footer() ).html(
                            '<span>Total Sales: </span>'+ totalSales
                        );
                    }
            });
            // Refilter the table
            $('#min, #max').on('change', function () {
                            table.draw();
                });
            });
            
         function total(){
             var qty = $("#quantity").val();
             var kg_price =  $("#kg_price").val();
             var total_weight =  $("#total_weight").val();
             var total = kg_price * total_weight;
             $("#total_price").val(total);

         }

    </script>

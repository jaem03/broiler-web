

        <!-- page content -->
    <div class="right_col" role="main">
        <div class = "container">

        </div>
            <h4>Production Record of Batch <?= $header ?> with total </h4>
     <div class="container">
            <?php if (session()->getFlashdata('success') !== NULL) : ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('success') ?>
            </div>
        <?php endif; ?>
        <?php if (session()->getFlashdata('error') !== NULL) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('error') ?>
                </div>
            <?php endif; ?>

    <table border="0" cellspacing="5" cellpadding="5" class = "table-bordered" style = "margin-bottom:10px;">
            <tbody><tr>
                <td>Record From:</td>
                <td><input type="text" id="min" name="min"></td>
            </tr>
            <tr>
                <td>To:</td>
                <td><input type="text" id="max" name="max"></td>
            </tr>
            </tbody>
    </table>
            <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Worker</th>
                <th>Mortality</th>
                <th>Feed Consumption</th>
                <th>Harvested Chicken</th>
                <th>Remarks</th>
                <th>Option</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($batch as $dt){ ?>
                <tr>
                    <td><?= $dt->date ?></td>
                    <td><?= $dt->worker ?></td>
                    <td><?= $dt->mortality ?></td>
                    <td><?= $dt->feed_consumption ?></td>
                    <td><?= $dt->harvested_chickens ?></td>
                    <td><?= $dt->remarks ?></td>
                    <td>
                    <a class = "btn btn-sm btn-danger" href= "<?= base_url('production/delete_batch_production/'.$dt->id.'/'.$dt->batch) ?>"> <i class = "fa fa-trash"></i> </a>
                    </td>
                </tr>
            <?php }?>
        </tbody>
        <tfoot>
            <tr>
            <th>Date</th>
                <th>Worker</th>
                <th>Mortality</th>
                <th>Feed Consumption</th>
                <th>Harvested Chicken</th>
                <th>Remarks</th>
                <th>Option</th>
            </tr>
        </tfoot>
    </table>
            </div>
        </div>
        <!-- /page content -->

        <!-- modals -->
        <div class="modal fade" id = "add-batch" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-sm" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add new Batch</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form id="demo-form2" method= "post" action = "<?= base_url('production/add-batch') ?>">
                        <div class = "row">
                            <div class="col-md-12 col-sm-12 ">
                                <input type="hidden" name= "building" id = "building" value = "<?= $header ?>">
                                <label for="">Batch</label>
                                <input type="text" id="batch" name = "batch" required="required" class="form-control ">
                            </div>
                            <div class="col-md-12 col-sm-12 ">
                                <label for="">Total Number of Chickens</label>
                                <input type="text" id="total_chickens" name = "total_chickens" required="required" class="form-control ">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                    </div>
                    </form>
                </div>
                </div>


    <script>
        var minDate, maxDate;
 
        // Custom filtering function which will search data in column four between two values
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                var min = minDate.val();
                var max = maxDate.val();
                var date = new Date( data[0] );
        
                if (
                    ( min === null && max === null ) ||
                    ( min === null && date <= max ) ||
                    ( min <= date   && max === null ) ||
                    ( min <= date   && date <= max )
                ) {
                    return true;
                }
                return false;
            }
        );
        $(document).ready(function() {
            minDate = new DateTime($('#min'), {
                    format: 'MMMM Do YYYY'
                });
                maxDate = new DateTime($('#max'), {
                    format: 'MMMM Do YYYY'
                });
 
            var table =  $('#example').DataTable();
            // Refilter the table
            $('#min, #max').on('change', function () {
                            table.draw();
                });
            });
            
         function total(){
             var qty = $("#quantity").val();
             var kg_price =  $("#kg_price").val();
             var total_weight =  $("#total_weight").val();
             var total = kg_price * total_weight;
             $("#total_price").val(total);

         }

    </script>

        <div class="col-md-3 left_col" >
            <div class="left_col scroll-view" style = "background-color: #34495e; ">
                <div class="navbar nav_title " style="border: 0;" >
                    <a href="<?= base_url('dashboard/index') ?>" class="site_title " style = "background-color:#ecf0f1">
                        <img width = "40" src="<?= base_url('assets/images/chicken.png') ?>" alt="" srcset="">
                        <span class = "text-dark">Broiler Manager</span>
                    </a>
                </div>

                <div class="clearfix"></div>

                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="<?= base_url('assets/images/user.png') ?>" class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome</span>
                        <h2>Juan Dela Cruz</h2>
                    </div>
                </div>

                <br>

                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu" >
                    <div class="menu_section">
                        <h3>General</h3>
                        
                        <ul class="nav sidebar-menu">
                            <li>
                                <a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>

                                
                                <ul class="nav child_menu">
                                    <li>
                                        <a href="<?= base_url('dashboard/index') ?>">Dashboard</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('dashboard/production') ?>">Production</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('dashboard/financial') ?>">Financial Management</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('dashboard/sales') ?>">Sales</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('dashboard/supplies') ?>">Supplies</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('dashboard/workers') ?>">Workers</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav sidebar-menu">
                            <li>
                                <a><i class="fa fa-file"></i> Reports <span class="fa fa-chevron-down"></span></a>

                                
                                <ul class="nav child_menu">
                                    <li>
                                        <a href="<?= base_url('reports/sales_report') ?>">Sales Report</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('reports/financial_report') ?>">Financial Report</a>
                                    </li>
                                   
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
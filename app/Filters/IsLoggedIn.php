<?php

namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class IsLoggedIn implements FilterInterface
{
	protected $session;
	public function __construct() {
		$this->session = \Config\Services::session();
	}

	public function before(RequestInterface $request, $arguments = null)
	{
		if ( $this->session->get('sess_logged_in') ) {
			return redirect()->to(base_url());
		}
	}
	

	public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
	{
		//
	}
}
